#!/usr/bin/env bash

echo -e "\e[1;35;40mRESTORE {etc,opt,profiles,Music,Videos}\e[0m"

RESTORE_FOLDER="/mnt/encrypted/$(hostname)"
FILE_ETC="files/etc"
FILE_OPT="files/opt"
FILE_PROFILES="files/profiles"

if [ -d "$RESTORE_FOLDER" ]; then
	if [ -f "$FILE_ETC" ]; then
		echo -e "\n\e[1;31mETC\e[0m"
		sudo rsync -ahrv --delete --files-from=$FILE_ETC \
		    $RESTORE_FOLDER/etc/ /etc/
	fi

	if [ -f "$FILE_OPT" ]; then
		echo -e "\n\e[1;31mOPT\e[0m"
		sudo rsync -ahrv --delete --files-from=$FILE_OPT \
		    $RESTORE_FOLDER/opt/ /opt/
	fi

	if [ -f "$FILE_PROFILES" ]; then
		echo -e "\n\e[1;31mPROFILES\e[0m"
		sudo rsync -ahrv --delete --files-from=$FILE_PROFILES \
		    $RESTORE_FOLDER/profiles/$USER/ /home/$USER/
	fi
fi
